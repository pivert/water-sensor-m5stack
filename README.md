# Water Sensor M5Stack & TL-136



## Introduction

Code pour [UIFlow](https://flow.m5stack.com/) pour mesurer la pression au fond d'un puit à l'aide d'un capteur TL-136, et en déduire la hauteur de la colonne d'eau. Vous pouvez directement importer le fichier dans l'[UIFlow](https://flow.m5stack.com/) - Pas besoin de s'enregistrer :
- Télécharger le fichier WaterSensor.m5f, et l'importer dans l'[UIFlow](https://flow.m5stack.com/)

## Fonctionnement

- La lecture étant particulièrement imprécise, je pense à cause du manque de stabilité de la valeur de référence du CAN du M5Stack, la valeur est lue 20000 fois, et on prend la moyenne.
- La valeur est transmise sur un serveur MQTT (Mosquitto), pour exploitation dans un Prometheus/Grafana. 
- Les valeurs sont mise à jour sur l'UI du M5Stack, et le diagramme du puit représentant la hauteur d'eau est redessiné.

Pour plus de détails, voir https://www.pivert.org/wp-admin/post.php?post=9084
