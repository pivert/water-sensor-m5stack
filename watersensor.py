#!/usr/bin/env python3
#
import paho.mqtt.client as mqttClient
import time

def on_connect(client, userdata, flags, rc):

    if rc == 0:
        print("Connected to broker")
        global Connected                #Use global variable
        Connected = True                #Signal connection

    else:
        print("Connection failed")

    client.subscribe("/M5Stack")

def text_exporter_write(value):
    with open('/var/lib/prometheus/node-exporter/water_level.prom', 'w') as f:
        f.write("# HELP water_level in well\n")
        f.write("# TYPE water_level gauge\n")
        f.write("water_level{origin=\"Water level sensor\"} " + str(value) + "\n")


def on_message(client, userdata, message):
    print(f"Message received: {message.payload}")
    text_exporter_write(float(message.payload))

Connected = False   #global variable for the state of the connection

broker_address= "swarm1"  #Broker address
port = 1883                          #Broker port
user = "promgw"                    #Connection username
password = "mqttpassword"            #Connection password

client = mqttClient.Client("Python")               #create new instance
client.username_pw_set(user, password=password)    #set username and password
client.on_connect= on_connect                      #attach function to callback
client.on_message= on_message                      #attach function to callback


connection_setup = False

while connection_setup != True:    #Wait for connection

    print("Connecting...")
    try:
        client.connect(broker_address, port=port)          #connect to broker
        connection_setup = True
    except ConnectionRefusedError:
        print("ConnectionRefusedError")

    time.sleep(2)


client.loop_forever()
